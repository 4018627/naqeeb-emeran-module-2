class App{
    var name;
    var category;
    var developer;
    var year;

    void upperCase(String string){
        print(string.toUpperCase());
    }
}

void main(){
    App nakedInsurance = new App();

    nakedInsurance.name = "Naked Insurance";
    nakedInsurance.category = "Best Financial Solution";
    nakedInsurance.developer =  "Sumarie Greybe, Ernest North and Alex Thomson";
    nakedInsurance.year = 2019;

    print("App name: ${nakedInsurance.name}");
    print("Category won: ${nakedInsurance.category}");
    print("Developer: ${nakedInsurance.developer}");
    print("Year won: ${nakedInsurance.year}\n");

    nakedInsurance.upperCase(nakedInsurance.name);
}